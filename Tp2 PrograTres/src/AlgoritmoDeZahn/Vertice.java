package AlgoritmoDeZahn;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Vertice {
	Coordinate coord;
	ArrayList <Vertice>vecino;
	double dist;
	
	public Vertice(Coordinate coord){
		this.coord = coord;
		this.vecino = new ArrayList<Vertice>();
		dist = 0;
	}
	
	public void agregarVecinos(int i, ArrayList<Vertice> vertices, double distMax){
		for (int j = i; j < vertices.size(); j++) {
			dist = DistanciaEuclidea(coord, vertices.get(j).coord);
			if(dist < distMax)
				vecino.add(vertices.get(j));
		}
	}
	
	public double DistanciaEuclidea(Coordinate coord1, Coordinate coord2) {
		return Math
				.sqrt(Math.pow(coord2.getLat() - coord1.getLat(), 2) + Math.pow(coord2.getLon() - coord1.getLon(), 2));

	}
	
	public void quitarVecino(){
		this.vecino=null;
	}
	
	public boolean vecinoNull(){
		if(this.vecino == null){
			return true;
		}
		return false;
	}
	
	public void agregarPeso(double peso){
		dist = peso;
	}
	
	public boolean esMenorQue(double distMax){
		boolean ret = dist < distMax;
		return ret; 
	}

	public boolean contieneCoord(Coordinate coordinate) {
		return (coord.getLat() == coordinate.getLat() && coord.getLon() == coordinate.getLon());
	}

	public boolean esVecinoDe(Vertice v) {
		return (coord.getLat() == v.coord.getLat() && coord.getLon() == v.coord.getLon());
	}
}
