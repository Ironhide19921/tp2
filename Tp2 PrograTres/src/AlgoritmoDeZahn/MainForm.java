package AlgoritmoDeZahn;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

public class MainForm {
	private JFrame frame;
	private JMapViewer miMapa;
	private AlgoritmoDeZahn mapa;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		miMapa = new JMapViewer();
		miMapa.setZoomContolsVisible(false);
		miMapa.setDisplayPositionByLatLon(-34.521, -58.7008, 11);

		// Ponemos un marcador! latitud, longitud
//		MapMarker marker = new MapMarkerDot(-34.521, -58.7008);
//		marker.getStyle().setBackColor(Color.RED);
//		miMapa.addMapMarker(marker);

		// Ahora un pol�gono!
		mapa = new AlgoritmoDeZahn();
		

		
		
		

//				MapPolygon poligono = new MapPolygonImpl(mapa.clusters.get(i).cluster);
//				miMapa.addMapPolygon(poligono);
			
			
		
		
		int suma = 0;
		
		for (int i = 0; i < mapa.clusters.size(); i++) {
			suma += mapa.clusters.get(i).cluster.size();
		}
		
		
		System.out.println("suma de vertices totales = " + mapa.vertices.size());

		System.out.println("suma de vertices en clusters  = " + suma);
		System.out.println(mapa.clusters.size());
		


		// Y un marcador en cada v�rtice del pol�gono!
		for (Coordinate c : mapa.coord){
			miMapa.addMapMarker(new MapMarkerDot(c));
		}
		
//		MapMarker marker = new MapMarkerDot(mapa.coordOrdenadas.get(0).getLat(), mapa.coordOrdenadas.get(0).getLon());
//		marker.getStyle().setBackColor(Color.RED);
//		miMapa.addMapMarker(marker);

		for (Coordinate c : mapa.clusters.get(0).coordenadas()) {
			MapMarker marker = new MapMarkerDot(c.getLat(), c.getLon());
			marker.getStyle().setBackColor(Color.RED);
			miMapa.addMapMarker(marker);
		}
		
//		MapMarker marker = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(0));
//		marker.getStyle().setBackColor(Color.BLUE);
//		miMapa.addMapMarker(marker);
//		
//		MapMarker marker1 = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(1));
//		marker1.getStyle().setBackColor(Color.WHITE);
//		miMapa.addMapMarker(marker1);
//		
//		MapMarker marker2 = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(2));
//		marker2.getStyle().setBackColor(Color.PINK);
//		miMapa.addMapMarker(marker2);
//		
//		MapMarker marker3 = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(3));
//		marker3.getStyle().setBackColor(Color.BLUE);
//		miMapa.addMapMarker(marker3);
//		
//		MapMarker marker4 = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(4));
//		marker4.getStyle().setBackColor(Color.WHITE);
//		miMapa.addMapMarker(marker4);
//		
//		MapMarker marker5 = new MapMarkerDot(mapa.clusters.get(0).coordenadas().get(5));
//		marker5.getStyle().setBackColor(Color.PINK);
//		miMapa.addMapMarker(marker5);
		
		
//		for (Cluster clus : mapa.clusters) {
//			for(Coordinate c : clus.cluster){
//			MapMarker marker = new MapMarkerDot(c.getLat(), c.getLon());
//			marker.getStyle().setBackColor(Color.RED);
//			miMapa.addMapMarker(marker);
//		}
//		}
//		MapMarker marker = new MapMarkerDot(mapa.coord.get(67).getLat(), mapa.coord.get(67).getLon());
//		marker.getStyle().setBackColor(Color.RED);
//		miMapa.addMapMarker(marker);

		frame.setContentPane(miMapa);
		
	}
	
}
