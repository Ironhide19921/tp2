package AlgoritmoDeZahn;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class JSONRead {

	   public ArrayList<Coordinate> LeerDeArchivo(){
		JSONParser parser = new JSONParser();
		ArrayList<Coordinate> coord = new ArrayList<Coordinate>();

		try {

			Object obj = parser.parse(new FileReader("C:\\Users\\German\\Desktop\\Universidad\\Programacion 3\\Trabajos practicos\\TP 2\\instancias\\instancia1.json"));
			
			JSONArray jsonArray = (JSONArray)obj;
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> iterator = jsonArray.iterator();

			while(iterator.hasNext()){
			    JSONObject jsonObject = iterator.next();
			    
			   
			    double lat = (double) jsonObject.get("latitud");
			    double lon = (double) jsonObject.get("longitud");
			    coord.add(new Coordinate(lat,lon)) ;
				
			}

		} catch (FileNotFoundException e) {
			//manejo de error
		} catch (IOException e) {
			//manejo de error
		} catch (ParseException e) {
			//manejo de error
		}
		return coord;
	}

}