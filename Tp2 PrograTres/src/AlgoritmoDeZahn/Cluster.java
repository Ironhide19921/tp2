package AlgoritmoDeZahn;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Cluster {

	ArrayList<Vertice> cluster;

	public Cluster() {
		cluster = new ArrayList<Vertice>();
	}

	public void agregarVertices(Vertice v){
		if(!contieneVertice(v)){
		cluster.add(v);
		}
		if(!puedeAgregarMas(v)){
			return;
		}
		agregarVertices(vecinoNoAgregado(v));
		
	}

	private Vertice vecinoNoAgregado(Vertice v) {
		for (Vertice vecino: v.vecino) {
			if(!contieneVertice(vecino))
				return vecino;
		}
		return null;
	}

	private boolean puedeAgregarMas(Vertice v) {
		if(v != null){
		boolean ret = false;
		for (Vertice vecino: v.vecino) {
			ret = ret || (!contieneVertice(vecino));
		}
		return ret;
		}
		return false;
	}

	public boolean contieneVertice(Vertice ver) {
		boolean ret = false;
		for (Vertice v : cluster) {
			ret = ret || sonIguales(v, ver);
		}
		return ret;
	}

	private boolean sonIguales(Vertice v1, Vertice v2) {
		return (v1.coord.getLat() == v2.coord.getLat() && v1.coord.getLon() == v2.coord.getLon());
	}
	
	public ArrayList<Vertice> getCluster() {
		System.out.println(cluster.size());
		return cluster;
	}
	
	public ArrayList<Coordinate> coordenadas(){
		ArrayList<Coordinate> c = new ArrayList<Coordinate>();
		for (Vertice v : cluster) {
			c.add(v.coord);
		}
		return c;
	}

	public int cantidadVertices() {
		return cluster.size();
	}

}
