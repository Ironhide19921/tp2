package AlgoritmoDeZahn;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class AlgoritmoDeZahn {
	
	JSONRead leer;
	ArrayList<Vertice> vertices;
	ArrayList<Coordinate> coord;
	double distMax;
	ArrayList<Cluster> clusters;

	public AlgoritmoDeZahn() {
		vertices = new ArrayList<Vertice>();
		leer = new JSONRead();
		coord = leer.LeerDeArchivo();
		clusters = new ArrayList<Cluster>();
		clusters.add(new Cluster());
		solver();
	}

	public void solver() {
		Coordinate coord = tomarExtremo();
		agregarVertices(coord);
		distanciaPromedio();
		crearAristas();
		crearClusters(verticeSinCluster(), 0);
		verCluster();
	}

	private Coordinate tomarExtremo() {
		Coordinate aux = new Coordinate(Double.MAX_VALUE, Double.MAX_VALUE);
		for (int i = 0; i < coord.size(); i++) {
			if (coord.get(i).getLon() < aux.getLon()) {
				aux = coord.get(i);
			}
		}

		return aux;
	}

	private void agregarVertices(Coordinate c) {
		if (vertices.size() == coord.size()) {
			return;
		}
		vertices.add(new Vertice(c));
		agregarVertices(masCercano(c));
	}

	private Coordinate masCercano(Coordinate c) {
		double save = Double.MAX_VALUE;
		Coordinate aux = null;
		for (Coordinate coor : coord) {

			if (!contieneCoord(coor)) {

				if (DistanciaEuclidea(c, coor) < save) {
					save = DistanciaEuclidea(c, coor);
					aux = coor;
				}
			}
		}
		return aux;
	}

	private boolean contieneCoord(Coordinate coordinate) {
		boolean ret = false;
		for (Vertice v : vertices) {
			ret = ret || v.contieneCoord(coordinate);
		}
		return ret;
	}

	private void crearAristas() {
		for (int i = 0; i < vertices.size(); i++) {
			vertices.get(i).agregarVecinos(i, vertices, distMax);
		}
	}

	public void distanciaPromedio() {
		for (int i = 0; i < vertices.size() - 1; i++) {
			distMax = distMax + DistanciaEuclidea(vertices.get(i).coord, vertices.get(i + 1).coord);

		}
		distMax = distMax / vertices.size();
		System.out.println(distMax);
	}

	public double DistanciaEuclidea(Coordinate coord1, Coordinate coord2) {
		return Math
				.sqrt(Math.pow(coord2.getLat() - coord1.getLat(), 2) + Math.pow(coord2.getLon() - coord1.getLon(), 2));

	}

	private void crearClusters(Vertice v, int k) {
		if (mismaCantidad()) {
			return;
		}
		clusters.get(k).agregarVertices(v); 
		clusters.add(new Cluster());
		crearClusters(verticeSinCluster(), k +=1);
	}

	private boolean mismaCantidad() {
		int cantVertices = 0;
		for (Cluster clus: clusters) {
			cantVertices += clus.cluster.size();
		}
		return cantVertices == vertices.size();
	}

	private Vertice verticeSinCluster() {
		for (Vertice v : vertices) {
			if (!tieneCluster(v)) {
				return v;
			}
		}
		return null;
	}

	private boolean tieneCluster(Vertice v) {
		boolean ret = false;
		for (Cluster cluster : clusters) {
			ret = ret || cluster.contieneVertice(v);
		}
		return ret;
	}
	
	public void verCluster(){
		for (int i = 0; i < clusters.size(); i++) {
			System.out.println("cluster["+ i +"] " + clusters.get(i).cantidadVertices());
		}
	}

	public Coordinate getCoord(int i) {
		return coord.get(i);
	}

	public Cluster getCluster(int i) {
		return clusters.get(i);
	}

}
